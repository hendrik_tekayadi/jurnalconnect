module WebhookModule

  def remove_webhooks(shop, id = nil)
    session = ShopifyAPI::Session.new(shop.shopify_domain, shop.shopify_token)
    ShopifyAPI::Base.activate_session(session)
    
    webhooks = ShopifyAPI::Webhook.find(:all)
    webhooks.each do |webhook|
      if id != nil && Rails.cache.read(shop.shopify_domain) != id
        # update_current_webhook_status(shop)
        webhook = ShopifyAPI::Webhook.create(:topic => "app/uninstalled", :address => ENV["UNINSTALL_PATH"], :format => "json")
        puts "Process Terminated"
        return false
      end

      if webhook.destroy
        puts "Webhook Destroyed!"
        puts "#{webhook.inspect}"
      end
    end
    webhook = ShopifyAPI::Webhook.create(:topic => "app/uninstalled", :address => ENV["UNINSTALL_PATH"], :format => "json")

    update_current_webhook_status(shop)
  end

  def add_webhooks(shop, id = nil)
    session = ShopifyAPI::Session.new(shop.shopify_domain, shop.shopify_token)
    ShopifyAPI::Base.activate_session(session)
    
    webhook_array = [
      ShopifyAPI::Webhook.new(:topic => "products/create", :address => ENV["PRODUCT_WEBHOOK_PATH"], :format => "json"),
      ShopifyAPI::Webhook.new(:topic => "products/update", :address => ENV["PRODUCT_WEBHOOK_PATH"], :format => "json"),
      ShopifyAPI::Webhook.new(:topic => "products/delete", :address => ENV["PRODUCT_WEBHOOK_PATH"], :format => "json"),
      ShopifyAPI::Webhook.new(:topic => "orders/create", :address => ENV["ORDER_WEBHOOK_PATH"], :format => "json"),
      ShopifyAPI::Webhook.new(:topic => "orders/paid", :address => ENV["ORDER_WEBHOOK_PATH"], :format => "json"),
      ShopifyAPI::Webhook.new(:topic => "orders/updated", :address => ENV["ORDER_WEBHOOK_PATH"], :format => "json"),
      ShopifyAPI::Webhook.new(:topic => "customers/create", :address => ENV["CUSTOMER_WEBHOOK_PATH"], :format => "json"),
      ShopifyAPI::Webhook.new(:topic => "customers/update", :address => ENV["CUSTOMER_WEBHOOK_PATH"], :format => "json"),
      ShopifyAPI::Webhook.new(:topic => "order_transactions/create", :address => ENV["TRANSACTION_WEBHOOK_PATH"], :format => "json"),
      ShopifyAPI::Webhook.new(:topic => "fulfillments/update", :address => ENV["FULFILLMENT_WEBHOOK_PATH"], :format => "json"),
      ShopifyAPI::Webhook.new(:topic => "app/uninstalled", :address => ENV["UNINSTALL_PATH"], :format => "json")
    ]

    webhook_array.each do |webhook|
      if id != nil && Rails.cache.read(shop.shopify_domain) != id
        # update_current_webhook_status(shop)
        puts "Process Terminated"
        return false
      end

      if webhook.save
        puts "Webhook Registered!"
        puts "#{webhook.inspect}"
      end
    end

    update_current_webhook_status(shop)
  end

  private
    def update_current_webhook_status(shop)
      count = ShopifyAPI::Webhook.count
      puts "Updating Webhook Status..."
      if count == 11
        puts "Webhook Status: True"
        shop.webhook_status = true
        shop.save
      else
        puts "Webhook Status: False"
        shop.webhook_status = false
        shop.save  
      end
    end
end