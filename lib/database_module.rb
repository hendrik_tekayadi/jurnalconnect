module DatabaseModule
  def update_sync_logs(kind, total, success, domain)
    sync_log = SyncLog.new(kind: kind, success: success, total: total, shop_domain: domain)
    sync_log.save
  end

  def update_sync_status(shop, status)
    if shop != nil
      shop.update(sync_status: status)
    end
  end

  def save_to_log(params)
    args = {api_key: params["api_key"], comp_name: params["name"], shop_domain: params["shop"]}
    log = Log.new(args)
    log.save
  end

  def update_shop(token, status)
    shop = Shop.find_by(shopify_token: token)
    if shop != nil
      shop.update(config_status: status)
    end
  end

  def log_sent_data(params)
    args = {kind: params["type"], created_at_shopify: params["created_at"], response: params["response"], shop_domain: params["shop_domain"]}
    puts args
    datum = SentDatum.new(args)
    datum.save
  end
end