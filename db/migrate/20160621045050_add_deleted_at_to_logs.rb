class AddDeletedAtToLogs < ActiveRecord::Migration
  def change
    add_column :logs, :deleted_at, :string
  end
end
