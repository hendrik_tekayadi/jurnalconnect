class AddConfigStatusToShops < ActiveRecord::Migration
  def change
    add_column :shops, :config_status, :boolean, default: false
  end
end
