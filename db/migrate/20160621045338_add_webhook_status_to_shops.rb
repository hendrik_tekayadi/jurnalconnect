class AddWebhookStatusToShops < ActiveRecord::Migration
  def change
    add_column :shops, :webhook_status, :boolean, default: true
  end
end
