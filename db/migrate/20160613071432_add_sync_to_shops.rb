class AddSyncToShops < ActiveRecord::Migration
  def change
  	add_column :shops, :sync_status, :boolean, default: false
  end
end
