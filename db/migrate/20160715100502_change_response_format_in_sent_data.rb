class ChangeResponseFormatInSentData < ActiveRecord::Migration
  def change
  	change_column :sent_data, :response, :text
  end
end
