class AddResponseToSentData < ActiveRecord::Migration
  def change
  	add_column :sent_data, :response, :string
  end
end
