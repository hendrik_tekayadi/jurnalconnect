class CreateLogs < ActiveRecord::Migration
  def change
  	create_table :logs do |t|
    	t.string :api_key
    	t.string :email
    	t.string :comp_name
    	t.timestamps
  	end
  end
end
