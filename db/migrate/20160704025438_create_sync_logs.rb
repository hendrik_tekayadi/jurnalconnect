class CreateSyncLogs < ActiveRecord::Migration
  def change
    create_table :sync_logs do |t|
      t.string :kind
      t.integer :success
      t.integer :total
      t.timestamp :deleted_at
      t.timestamps null: false
    end
  end
end
