# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160704025923) do

  create_table "logs", force: :cascade do |t|
    t.string   "api_key",     limit: 255
    t.string   "comp_name",   limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "shop_domain", limit: 255
    t.string   "deleted_at",  limit: 255
  end

  create_table "sent_data", force: :cascade do |t|
    t.string   "kind",               limit: 255
    t.string   "created_at_shopify", limit: 255
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "response",           limit: 255
    t.string   "shop_domain",        limit: 255
    t.string   "deleted_at",         limit: 255
  end

  create_table "shops", force: :cascade do |t|
    t.string   "shopify_domain", limit: 255,                 null: false
    t.string   "shopify_token",  limit: 255,                 null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "api_key",        limit: 255
    t.boolean  "sync_status",                default: false
    t.boolean  "config_status",              default: false
    t.string   "deleted_at",     limit: 255
    t.boolean  "webhook_status",             default: true
  end

  add_index "shops", ["shopify_domain"], name: "index_shops_on_shopify_domain", unique: true, using: :btree

  create_table "sync_logs", force: :cascade do |t|
    t.string   "kind",        limit: 255
    t.integer  "success",     limit: 4
    t.integer  "total",       limit: 4
    t.datetime "deleted_at"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "shop_domain", limit: 255, null: false
  end

end
