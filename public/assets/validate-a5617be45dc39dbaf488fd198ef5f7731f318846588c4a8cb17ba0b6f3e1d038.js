function validateForm() {
  var api_key = document.forms["configForm"]["api_key"].value;
  var name = document.forms["configForm"]["name"].value;
  if(api_key == null || api_key == "") {
    alert("Please Insert Your API Key");
    return false;
  }
  if(!validateAPI(api_key)) {
    alert("Wrong API Key");
    return false;
  }
}

function validateAPI(key) {
  var re = /^[0-9a-fA-F]{32}$/;
  return re.test(key);
}
