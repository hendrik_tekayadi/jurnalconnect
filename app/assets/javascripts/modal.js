var sync_modal = document.getElementById('sync_information');
var fail_modal = document.getElementById('fail_information');

$("#close_sync_modal").click(function(event) {
  $("#sync_information").modal('hide');
});

$("#close_fail_modal").click(function(event) {
  $("#fail_information").modal('hide');
});

$("#confirm_sync").click(function(){
  $("#sync_information").modal('hide');
});

$("#confirm_fail").click(function(){
  $("#fail_information").modal('hide');
});

window.onclick = function(event) {
  if (event.target == sync_modal) {
    $("#sync_information").modal('hide');
  }
  if (event.target == fail_modal) {
    $("#fail_information").modal('hide');
  }
}