class ShopsController < ShopifyApp::AuthenticatedController
  include WebhookModule
  include ConnectionModule
  include DatabaseModule

  before_filter :prepare_notifier
  before_action :init_shop
  before_action :init_company_name, :only => [:show, :edit]

  def update_table
    init_table
  end

  def login
    
  end

  def setting
    if @shop
      if @shop.config_status == true
        redirect_to shop_path(@shop)
      end
    end
  end

  def create
    if @shop
      update_shop(params["shop_token"], true)
      if params["sync"]
        update_sync_status(@shop, true)
        ShopSyncJob.perform_later(@shop)
        flash[:notice] = "Syncing in Background.."
      else
        flash[:notice] = "Configuration Successfully Saved"
      end
      
      save_to_log(params)
      add_webhooks(@shop)
      redirect_to shop_path(@shop)
    else
      redirect_to :back
    end

  rescue Exception => e
    puts e.message
    puts e.backtrace.inspect
    if @shop
      update_shop(params["shop_token"], false)
    end
    
    flash[:notice] = "Something went wrong, please try again"
    redirect_to :back
  end

  def show
    if @company_name.nil?
      @company_name = ""
    end
    init_table
  end

  def edit
    if @company_name.nil?
      @company_name = ""
    end

    @checked = false
    if @shop != nil
      if @shop.webhook_status
        @checked = true
      end
    end
    session[:return_to] ||= request.referer
  end

  def update
    if @shop != nil
      if params["webhook_checkbox"] == "true"
        WebhookChangeJob.perform_later("add", @shop)
        flash[:notice] = "Webhook Updated"
      else
        WebhookChangeJob.perform_later("remove", @shop)
        flash[:notice] = "Webhook Updated"  
      end
    end
    
    render nothing: true
  end

  def destroy #Not being used
    if @shop != nil
      SentDatum.delete_all(shop_domain: "#{ShopifyApp::SessionRepository.retrieve(session[:shopify]).url}")
      SyncLog.delete_all(shop_domain: "#{ShopifyApp::SessionRepository.retrieve(session[:shopify]).url}")
      remove_webhooks(@shop)
      @shop.delete
    end

    webhook = ShopifyAPI::Webhook.new(:topic => "app/uninstalled", :address => ENV["UNINSTALL_PATH"], :format => "json")
    webhook.save
    flash[:notice] = "All Data Cleaned Successfully"
    
    redirect_to root_path, :shop => "#{ShopifyApp::SessionRepository.retrieve(session[:shopify]).url}"
  end

  def health
    render nothing: true
  end

  private
    def init_table
      @table_data = Array.new
      @last_datum = SentDatum.where(["(response = ? OR response = ?) AND shop_domain = ?", "201  Created", "200  OK", "#{ShopifyApp::SessionRepository.retrieve(session[:shopify]).url}"]).last
      if @last_datum != nil
        @update_time = @last_datum.updated_at.strftime("%A, %d %B %Y, %H:%M:%S")
        @table_data = SentDatum.where(["created_at_shopify != ? AND shop_domain = ? AND kind NOT LIKE ?", "NULL", "#{ShopifyApp::SessionRepository.retrieve(session[:shopify]).url}", "update%"]).order(created_at: :desc).first(10)
        
        @fail_array = Array.new
        @table_data.each do |datum|
          if datum.response != "200  OK" && datum.response != "201  Created"
            temp_hash = {
              "kind" => "#{datum.kind}",
              "response" => "#{datum.response}"
            }
            @fail_array.push(temp_hash)
          end
        end
      end

      if SyncLog.find_by(shop_domain: "#{ShopifyApp::SessionRepository.retrieve(session[:shopify]).url}") && @table_data.count < 10
        product = get_sync_log("Sync Products")
        customer = get_sync_log("Sync Customers")
        order = get_sync_log("Sync Orders")
        @step_count = 0
        @success_count = 0
        sync_hash = {
          "type" => "sync_data",
          "message" => "Synced: "
        }

        if product
          product_log_item = SentDatum.find_by(kind: "Create product (batch)", shop_domain: "#{ShopifyApp::SessionRepository.retrieve(session[:shopify]).url}")
          if product_log_item.nil?
            @product_message = "No Product To Sync"
          else
            @product_message = product_log_item.response
          end
          
          @sync_product_total = product.total
          @sync_product_success = product.success
          sync_hash["message"] += "Products #{@sync_product_success} out of #{@sync_product_total}"
          sync_hash["date"] = product.updated_at
          @step_count += 1

          if @sync_product_success == @sync_product_total
            @success_count += 1
          end
        end

        if customer
          customer_log_item = SentDatum.find_by(kind: "Create customer (batch)", shop_domain: "#{ShopifyApp::SessionRepository.retrieve(session[:shopify]).url}")
          if customer_log_item.nil?
            @customer_message = "No Customer To Sync"
          else
            @customer_message = customer_log_item.response
          end
          @sync_customer_total = customer.total
          @sync_customer_success = customer.success
          sync_hash["message"] += ", Customers #{@sync_customer_success} out of #{@sync_customer_total}"
          sync_hash["date"] = customer.updated_at
          @step_count += 1

          if @sync_customer_success == @sync_customer_total
            @success_count += 1
          end
        end

        if order
          order_log_item = SentDatum.find_by(kind: "Create order (batch)", shop_domain: "#{ShopifyApp::SessionRepository.retrieve(session[:shopify]).url}")
          if order_log_item.nil?
            @order_message = "No Order To Sync"
          else
            @order_message = order_log_item.response
          end
          @sync_order_total = order.total
          @sync_order_success = order.success
          sync_hash["message"] += ", Orders #{@sync_order_success} out of #{@sync_order_total}"
          sync_hash["date"] = order.updated_at
          @step_count += 1

          if @sync_order_success == @sync_order_total
            @success_count += 1
          end
        end

        if @success_count == 3
          @icon = "glyphicon-ok-sign"
          @color = "green"
        elsif @success_count == 0
          @icon = "glyphicon-remove-sign"
          @color = "red"
        else
          @icon = "glyphicon-ok-sign"
          @color = "#FFCF00" # Dark yellow
        end

        if @table_data != nil
          @table_data << sync_hash
        end
      end

      if @shop != nil
        test = @table_data.last

        if @shop.sync_status && test != nil && test["type"] == "sync_data"
          @sync = true
        end
      end
    end

    def init_company_name
      if Log.where(shop_domain: "#{ShopifyApp::SessionRepository.retrieve(session[:shopify]).url}").select("comp_name").last != nil
        @company_name = Log.where(shop_domain: "#{ShopifyApp::SessionRepository.retrieve(session[:shopify]).url}").select("comp_name").last.comp_name
      end
    end

    def init_shop
      @shop = Shop.find_by(shopify_token: "#{ShopifyApp::SessionRepository.retrieve(session[:shopify]).token}")
    end

    def prepare_notifier
      shop = Shop.find_by(shopify_token: "#{ShopifyApp::SessionRepository.retrieve(session[:shopify]).token}")
      if shop
        request.env["exception_notifier.exception_data"] = {
          :current_shop => shop.shopify_domain  
        }
      end
    end

    def get_sync_log(kind)
      SyncLog.where(["kind = ? AND shop_domain = ?", "#{kind}", "#{ShopifyApp::SessionRepository.retrieve(session[:shopify]).url}"]).last
    end
end