class HomeController < ShopifyApp::AuthenticatedController
  def index
    @shop = Shop.find_by(shopify_token: "#{ShopifyApp::SessionRepository.retrieve(session[:shopify]).token}")
    unless @shop.api_key.nil?
      @authorized = true
    end
  end

  def callback
    @shop = Shop.find_by(shopify_token: "#{ShopifyApp::SessionRepository.retrieve(session[:shopify]).token}")
    if params["access_token"]
      @shop.api_key = params["access_token"]
      @shop.save
    end
    redirect_to root_path
  end
end