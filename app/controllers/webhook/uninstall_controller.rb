class Webhook::UninstallController < ApplicationController
  include ConnectionModule
  include DatabaseModule
  include ShopifyApp::WebhookVerification

  before_filter :prepare_notifier

  def create
    Shop.delete_all(shopify_domain: request.headers["HTTP_X_SHOPIFY_SHOP_DOMAIN"])
    SentDatum.delete_all(shop_domain: request.headers["HTTP_X_SHOPIFY_SHOP_DOMAIN"])
    SyncLog.delete_all(shop_domain: request.headers["HTTP_X_SHOPIFY_SHOP_DOMAIN"])
    head :ok
  end

  private
    def prepare_notifier
      shop = request.headers["HTTP_X_SHOPIFY_SHOP_DOMAIN"]
      if shop != nil
        request.env["exception_notifier.exception_data"] = {
          :current_shop => shop  
        }
      end
    end
end