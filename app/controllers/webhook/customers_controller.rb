class Webhook::CustomersController < ApplicationController
  include ConnectionModule
  include DatabaseModule
  include ShopifyApp::WebhookVerification

  before_filter :prepare_notifier

  def create
    webhook_job_klass.perform_later(customers_params, request.headers["HTTP_X_SHOPIFY_SHOP_DOMAIN"])
    head :ok
  end

  private

  def prepare_notifier
    shop = request.headers["HTTP_X_SHOPIFY_SHOP_DOMAIN"]
    if shop != nil
      request.env["exception_notifier.exception_data"] = {
        :current_shop => shop  
      }
    end
  end

  def customers_params
    params.permit!
  end

  def webhook_job_klass
    "#{webhook_type.classify}Job".safe_constantize
  end

  def webhook_type
    request.headers["HTTP_X_SHOPIFY_TOPIC"].gsub! '/', '_'
  end
end