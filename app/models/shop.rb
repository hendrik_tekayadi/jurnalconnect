class Shop < ActiveRecord::Base
  include ShopifyApp::Shop
  include ShopifyApp::SessionStorage
  include ConnectionModule
  include DatabaseModule

  def sync_all_data
    sync_product
    sync_customer
    sync_order
  end

  def delete
    self.api_key = nil
    self.config_status = false
    self.webhook_status = true
    self.sync_status = false
    self.save
  end

  def create_payment(params)
    order = ShopifyAPI::Session.temp(self.shopify_domain, self.shopify_token){
      ShopifyAPI::Order.find(params["order_id"])
    }
    if order != nil
      order_hash = JSON.parse(order.to_json)
      sales_hash = JSON.parse(request_from_jurnal(ENV["SALES_PATH"], self.shopify_domain).body)
      sales = sales_hash["sales_invoices"]
      if sales != nil
        if !sales.empty?
          target = sales.find {|x| x["transaction_no"] == order_hash["order_number"].to_s}
          if !target.nil?
            send_payload(ENV["PAYMENT_PATH"], payment_payload(params, order_hash), "payment (#{order_hash["order_number"].to_s})", "post")
          end
        end
      end
    end
  end

  def create_customer(params, http_type)
    @targetURL = ENV["CUSTOMER_PATH"]
    @type = "post"
    
    key = params["last_name"]
    if params["first_name"] != "" && params["first_name"] != nil
      key = "#{params["first_name"]} #{params["last_name"]}"
    end
    
    if http_type == "put"
      customers_hash = JSON.parse(request_from_jurnal(ENV["CUSTOMER_PATH"], self.shopify_domain).body)
      customers = customers_hash["customers"]
      if customers != nil
        if !customers.empty?
          target = customers.find {|x| x["display_name"] == key}
          if !target.nil?
            @targetURL += "/#{target["id"]}"
            @type = "put"
          end
        end
      end
    end
    send_payload(@targetURL, customer_payload(params), "Customer (#{key})", @type)
  end

  def create_product(params, http_type)
    variants = params["variants"]
    variants.each do |variant|
      @targetURL = ENV["PRODUCT_PATH"]
      @type = "post"
      @send_quantity = true
      key = variant["sku"]
      if http_type == "put"
        products_hash = JSON.parse(request_from_jurnal(ENV["PRODUCT_PATH"], self.shopify_domain).body)
        products = products_hash["products"]
        if !products.empty?
          target = products.find {|x| x["product_code"] == key}
          if !target.nil?
            @targetURL += "/#{target["id"]}"
            @type = "put"
            @send_quantity = false
          end
        end
      end
      if variant["title"] == "Default Title"
        title = params["title"]
      else
        title = "#{params["title"]} - #{variant["title"]}"
      end
      send_payload(@targetURL, product_payload(params, variant, @send_quantity), "Product (#{title})", @type)
    end
  end

  def create_order(params, http_type)
    @targetURL = ENV["SALES_PATH"]
    @type = "post"
    key = params["order_number"].to_s
    if http_type == "put"
      sales_hash = JSON.parse(request_from_jurnal(ENV["SALES_PATH"], self.shopify_domain).body)
      sales = sales_hash["sales_invoices"]
      if sales != nil
        if !sales.empty?
          target = sales.find {|x| x["transaction_no"] == key}
          if !target.nil?
            @targetURL += "/#{target["id"]}"
            @type = "put"
          end
        end
      end
    end
    send_payload(@targetURL, order_payload(params, @type), "Order - #{key}", @type)
  end

  private

    def payment_payload(params, order_hash)
      if params["kind"] == "sale" || params["kind"] == "capture"
        name = ""
        if params["kind"] == "sale"
          @method = "Cash"
          @account = "Cash"
        else
          @method = "Credit Card"
          @account = "Bank Account"
        end

        @record_array = Array.new
        if order_hash != nil
          name = "Default Customer - shopify"
          if order_hash["customer"] != nil
            name = order_hash["customer"]["last_name"]
            if order_hash["customer"]["first_name"] != "" && order_hash["customer"]["first_name"] != nil
              name = "#{order_hash["customer"]["first_name"]} #{order_hash["customer"]["last_name"]}"
            end
          end

          @record = {
            "transaction_no" => order_hash["order_number"].to_s,
            "amount" => params["amount"].to_f
          }
          @record_array.push(@record)
        end
        item = {
          "person_name" => name,
          "transaction_no" => params["id"].to_s,
          "transaction_date" => params["created_at"],
          "currency_list_name" => params["currency"],
          "payment_method_name" => @method,
          "deposit_to_name" => @account,
          "memo" => params["message"],
          "records_attributes" => @record_array,
          "source" => "shopify"
        }
      end

      payload = {
        "receive_payment" => item,
        "created_at" => params["created_at"]
      }
    end

    def customer_payload(params)
      display_name = params["last_name"]
      if params["first_name"] != "" && params["first_name"] != nil
        display_name = "#{params["first_name"]} #{params["last_name"]}"
      end
      item = {
        "first_name" => params["first_name"],
        "last_name" => params["last_name"],
        "display_name" => display_name,
        "email" => params["email"],
        "source" => "shopify"
      }

      if params["addresses"] != nil
        addresses = params["addresses"]
        addresses.each do |address|
          if address["default"]
            @def_address = JSON.parse(address.to_json)
          end
        end

        if !@def_address.nil?
          @def_address.each {|key, value| @def_address[key] = "" if value.nil?}

          item[:billing_address] = @def_address["address1"] + " " + @def_address["address2"] + " " + @def_address["city"] + " " + @def_address["province"] + " " + @def_address["country_name"] + " " + @def_address["zip"]
          item[:address] = @def_address["address1"] + " " + @def_address["address2"] + " " + @def_address["city"] + " " + @def_address["province"] + " " + @def_address["country_name"] + " " + @def_address["zip"]
          item[:phone] = @def_address["phone"]
        end
      end

      payload = {
        "customer" => item,
        "created_at" => params["created_at"]
      }
    end

    def product_payload(params, variant, send_quantity)
      if variant["title"] == "Default Title"
        title = params["title"]
      else
        title = "#{params["title"]} - #{variant["title"]}"
      end
      item = {
        "name" => title,
        "description" => params["body_html"],
        "product_code" => variant["sku"],
        "unit_name" => "Pcs",
        "sell_price_per_unit" => variant["price"],
        "taxable_sell" => variant["taxable"],
        "is_bought" => false,
        "source" => "shopify"
      }

      if params["product_type"] != ""
        create_category(params["product_type"])
        tempArr = Array.new()
        tempArr.push(params["product_type"])
        item[:product_category_names] = tempArr
      end

      
      if send_quantity
        item[:track_inventory] = "true"
        item[:init_price] = 0
        item[:init_quantity] = 0
        item[:init_date] = params["created_at"]
        item[:inventory_asset_account_number] = "1-1400"

        if (variant["inventory_management"] == "shopify")
          item[:init_quantity] = variant["inventory_quantity"]
        end
      end

      payload = {
        "product" => item,
        "created_at" => params["created_at"]
      }
    end

    def order_payload(params, type = "post")
      @is_paid = false
      billing_addr = params["billing_address"]
      shipping_addr = params["shipping_address"]
      customer = params["customer"]
      line_items = params["line_items"]
      fulfillments = params["fulfillments"]

      if billing_addr.nil?
        @billing = "Default Billing Address"
        @billing_country = "Default Country"
      else
        @billing = "#{billing_addr["address1"]} #{billing_addr["city"]} #{billing_addr["province"]} #{billing_addr["country"]} #{billing_addr["zip"]}"
        @billing_country = "#{billing_addr["country"]}"
      end

      unless customer.nil?
        @display_name = customer["last_name"]
        if customer["first_name"] != "" && customer["first_name"] != nil
          @display_name = "#{customer["first_name"]} #{customer["last_name"]}"
        end
      end

      if shipping_addr.nil?
        @shipping = "Default Shipping Address"
      else
        @shipping = "#{shipping_addr["address1"]} #{shipping_addr["city"]} #{shipping_addr["country"]} #{shipping_addr["zip"]}"
      end

      if params["email"] == nil || params["email"] == ""
        @email = ""
      else
        @email = params["email"]
      end

      if params["financial_status"] == "paid"
        @is_paid = true
      end

      item = {
        "person_name" => @display_name,
        "email" => @email,
        "address" => @billing,
        "shipping_address" => @shipping,
        "transaction_date" => params["created_at"],
        "is_shipped" => line_items[0]["requires_shipping"],
        "transaction_no" => params["order_number"].to_s,
        "deposit_to_name" => "Cash",
        "tax_name" => "",
        "memo" => params["note"],
        "is_paid" => @is_paid,
        "source" => "shopify"
      }

      # discount = 0
      # if !params["discount_codes"].nil?
      #   discounts = params["discount_codes"]
      #   discounts.each do |x|
      #     if x["type"] == "percentage"
      #       discount += x["amount"].to_i
      #     end

      #     if x["type"] == "fixed_amount"
      #       item[:discount_unit] += x["amount"]
      #       item[:discount_type_id] = 2
      #     end
      #   end
      # end

      shipping_fee = 0
      if line_items.select {|x| x[:requires_shipping]}
        shippings = params["shipping_lines"]
        if shippings != nil
          shippings.each do |shipping|
            shipping_fee += shipping["price"].to_i
          end
          item[:shipping_price] = shipping_fee 
        end
      end

      tempArr = Array.new
      tax_array = Array.new
      first = true
      count_tax = true
      tax_item_price = 0
      tax_item_desc = ""

      line_items.each do |product|
        if product["taxable"]
          if product["tax_lines"].nil?
            count_tax = false
          else
            product_taxes = Array.new
            taxes = product["tax_lines"]
            taxes.each do |tax|
              if count_tax
                if first
                  tax["total_price"] = product["price"].to_i * product["quantity"].to_i
                  tax_array.push(tax)
                else
                  product_taxes.push(tax)
                end
              end

              tax_item_price += tax["price"].to_i
              tax_item_desc += "#{tax["title"]} "
            end
            first = false
            if !product_taxes.empty?
              product_taxes.each do |x|
                found = false
                tax_array.each do |y|
                  if x["title"] == y["title"] && x["rate"] == y["rate"]
                    found = true
                  end
                end
                if found == false
                  count_tax = false
                end
              end
            end
          end
        end
      end

      if count_tax
        array_name = Array.new
        array_compound = Array.new
        name = ""
        if !tax_array.empty?
          tax_array.each do |tax|
            name = "#{tax["title"]} - #{@billing_country}"
            create_tax(name, tax["rate"] * 100)
            if (tax["price"].to_f / tax["total_price"].to_f) != tax["rate"]
              array_compound.push(name)
            end
            array_name.push(name)
          end
          if tax_array.count > 1
            name = "Group for Transaction #{params["order_number"].to_s}"
            create_tax_group(name, array_name, array_compound)
          end
          item[:tax_name] = name
        end
      else
        tempHash = {
          "product_name" => "Total Pajak",
          "description" => tax_item_desc,
          "quantity" => 1,
          "rate" => tax_item_price,
          "amount" => tax_item_price,
          "tax" => false
        }
        tempArr.push(tempHash)
      end

      if params["fulfillment_status"] == "fulfilled"
        fulfillments = params["fulfillments"]
        fulfillment = fulfillments[0]
        
        item[:ship_via] = ""
        item[:tracking_no] = ""

        if fulfillment["tracking_company"] != nil
          item[:ship_via] = fulfillment["tracking_company"]
        end

        if fulfillment["tracking_number"] != nil
          item[:tracking_no] = fulfillment["tracking_number"]
        end
      end

      @desc = "Product deleted at shopify"
      line_items.each do |product|
        ShopifyAPI::Session.temp(self.shopify_domain, self.shopify_token) {
          if ShopifyAPI::Product.exists?(product["product_id"])
            @product_item = ShopifyAPI::Product.find(product["product_id"])
            @desc = @product_item.body_html
          end
        }

        if product["variant_title"] == "default" || product["variant_title"] == "" || product["variant_title"] == nil
          @name = product["title"]
        else
          @name = "#{product["title"]} - #{product["variant_title"]}"
        end

        tempHash = {
          "product_name" => @name,
          "description" => @desc,
          "quantity" => product["quantity"],
          "rate" => product["price"].to_i,
          "amount" => product["price"].to_i * product["quantity"].to_i,
          "discount" => product["total_discount"].to_i * 100 / (product["price"].to_i * product["quantity"].to_i),
          "tax" => product["taxable"]
        }
        if !count_tax
          tempHash["tax"] = false
        end
        tempArr.push(tempHash)
      end
      item[:discount_unit] = params["total_discounts"].to_i
      item[:discount_type_id] = 2
      if type == "post"
        item[:transaction_lines_attributes] = tempArr
      end

      payload = {
        "sales_invoice" => item,
        "created_at" => params["created_at"]
      }
    end

    def send_payload(targetURL, payload, req_type, http_type)
      response = sendJSON(targetURL, payload.to_json, self.shopify_domain, http_type)
      puts "Response from Jurnal \n"
      puts "Response #{response.code} #{response.message} : #{response.body} \n"

      if http_type == "post"
        @type = "Create #{req_type}"
      else
        @type = "Update #{req_type}"
      end

      args = {
          "type" => @type,
          "created_at" => payload["created_at"],
          "response" => "#{response.code}  #{response.message}",
          "shop_domain" => self.shopify_domain
      }
      temp_array = req_type.split
      if temp_array.last != "(batch)"
        if response.code != "200" && response.code != "201"
          args["response"] = "#{response.body}"
        end
      end
      log_sent_data(args)
      return response
    end

    def create_tax(name, rate)
      tax_hash = JSON.parse(request_from_jurnal(ENV["TAX_PATH"], self.shopify_domain).body)
      taxes = tax_hash["company_taxes"]
      if !taxes.nil?
        if taxes.select {|x| x["name"] == name}.empty?
          item = {
            "name" => name,
            "rate" => rate
          }
          payload = {
            "tax" => item
          }
          thepost = sendJSON(ENV["TAX_PATH"], payload.to_json, self.shopify_domain, "post")
          puts "Response from Jurnal \n"
          puts "Response #{thepost.code} #{thepost.message} : #{thepost.body} \n"
          
          args = {
              "type" => "create_tax",
              "created_at" => nil,
              "response" => "#{thepost.code}  #{thepost.message}",
              "shop_domain" => self.shopify_domain
          }
          log_sent_data(args)
        end
      end
    end

    def create_tax_group(name, child_names, compount_child = [])
      item = {
        "name" => name,
        "child_names" => child_names,
        "compount_child" => compount_child
      }
      payload = {
        "tax" => item
      }
      thepost = sendJSON(ENV["TAX_PATH"], payload.to_json, self.shopify_domain, "post")
      puts "Response from Jurnal \n"
      puts "Response #{thepost.code} #{thepost.message} : #{thepost.body} \n"
      
      args = {
          "type" => "create_tax_group",
          "created_at" => nil,
          "response" => "#{thepost.code}  #{thepost.message}",
          "shop_domain" => self.shopify_domain
      }
      log_sent_data(args)
    end

    def create_unit(name)
      unit_hash = JSON.parse(request_from_jurnal(ENV["UNIT_PATH"], self.shopify_domain).body)
      units = unit_hash["product_units"]
      if !units.nil?
        if units.select {|x| x["name"] == name}.empty?
          item = {
              "name" => name
          }
          payload = {
              "product_unit" => item
          }
          thepost = sendJSON(ENV["UNIT_PATH"], payload.to_json, self.shopify_domain, "post")
          puts "Response from Jurnal \n"
          puts "Response #{thepost.code} #{thepost.message} : #{thepost.body} \n"
          
          args = {
              "type" => "create_unit",
              "created_at" => nil,
              "response" => "#{thepost.code}  #{thepost.message}",
              "shop_domain" => self.shopify_domain
          }
          log_sent_data(args)
        end
      end
    end

    def create_category(name)
      category_hash = JSON.parse(request_from_jurnal(ENV["CATEGORY_PATH"], self.shopify_domain).body)
      if category_hash[0] != "You're not authorized to perform this action." && category_hash[0] != "Anda tidak memiliki wewenang untuk mengakses."
        categories = category_hash["product_categories"]
        if !categories.nil?
          if categories.select {|x| x["name"] == name}.empty?
            item = {
                "name" => name
            }
            payload = {
                "product_category" => item
            }
            thepost = sendJSON(ENV["CATEGORY_PATH"], payload.to_json, self.shopify_domain, "post")
            puts "Response from Jurnal \n"
            puts "Response #{thepost.code} #{thepost.message} : #{thepost.body} \n"

            args = {
                "type" => "create_category",
                "created_at" => nil,
                "response" => "#{thepost.code}  #{thepost.message}",
                "shop_domain" => self.shopify_domain
            }
            log_sent_data(args)
          end
        end
      end
    end

    def sync_product
      page = 1
      total_count = 0
      success_count = 0
      item_count = 0
      products_array = Array.new
      count = ShopifyAPI::Session.temp("#{self.shopify_domain}", "#{self.shopify_token}") {ShopifyAPI::Product.count}
      puts "Total products: #{count}"
      
      if count > 0
        page += count.divmod(20).first
        fail_array = Array.new

        while page > 0
          puts "Now processing page: #{page}"
          products = ShopifyAPI::Session.temp("#{self.shopify_domain}", "#{self.shopify_token}") {ShopifyAPI::Product.find(:all, :params => {:page => page, :limit => 20})}
          products.each do |product|
            params = JSON.parse(product.to_json)
            variants = params["variants"]
            variants.each do |variant|
              products_array.push(product_payload(params, variant, true))
              total_count += 1
              item_count += 1
              
              if item_count == 20
                payload = {
                  "products" => products_array
                }
                response = send_payload(ENV["PRODUCT_BATCH_PATH"], payload, "product (batch)", "post")
                if response.code.to_s == "200"
                  response_array = JSON.parse(response.body)["products"]
                  response_array.each do |response_item|
                    if response_item["product"]["status"] == 201
                      success_count += 1
                    else
                      fail_array.push(response_item)
                    end
                  end
                end

                item_count = 0
                products_array.clear
              end
            end
          end

          if item_count > 0
            payload = {
              "products" => products_array
            }
            response = send_payload(ENV["PRODUCT_BATCH_PATH"], payload, "product (batch)", "post")
            if response.code.to_s == "200"
              response_array = JSON.parse(response.body)["products"]
              response_array.each do |response_item|
                if response_item["product"]["status"] == 201
                  success_count += 1
                else
                  fail_array.push(response_item)
                end
              end
            end  
          end

          page -= 1
          products_array.clear
        end

        unless fail_array.empty?
          product_log_item = SentDatum.find_by(kind: "Create product (batch)", shop_domain: "#{self.shopify_domain}")
          product_log_item.response = fail_array
          product_log_item.save
        end
      end

      update_sync_logs("Sync Products", total_count, success_count, self.shopify_domain)
    end

    def sync_customer
      page = 1
      total_count = 0
      success_count = 0
      customers_array = Array.new
      count = ShopifyAPI::Session.temp("#{self.shopify_domain}", "#{self.shopify_token}") {ShopifyAPI::Customer.count}
      puts "Total customers: #{count}"
      
      if count > 0
        page += count.divmod(15).first
        fail_array = Array.new

        while page > 0
          puts "Now processing page: #{page}"
          customers = ShopifyAPI::Session.temp("#{self.shopify_domain}", "#{self.shopify_token}") {ShopifyAPI::Customer.find(:all, :params => {:page => page, :limit => 15})}
          customers.each do |customer|
            params = JSON.parse(customer.to_json)
            customers_array.push(customer_payload(params))
            total_count += 1  
          end

          payload = {
            "customers" => customers_array
          }
          response = send_payload(ENV["CUSTOMER_BATCH_PATH"], payload, "customer (batch)", "post")
          if response.code.to_s == "200"
            response_array = JSON.parse(response.body)["customers"]
            response_array.each do |response_item|
              if response_item["customer"]["status"] == 201
                success_count += 1
              else
                fail_array.push(response_item)
              end
            end
          end

          page -= 1
          customers_array.clear
        end

        unless fail_array.empty?
          customer_log_item = SentDatum.find_by(kind: "Create customer (batch)", shop_domain: "#{self.shopify_domain}")
          customer_log_item.response = fail_array
          customer_log_item.save
        end
      end

      update_sync_logs("Sync Customers", total_count, success_count, self.shopify_domain)  
    end

    def sync_order
      page = 1
      total_count = 0
      success_count = 0
      orders_array = Array.new
      count = ShopifyAPI::Session.temp("#{self.shopify_domain}", "#{self.shopify_token}") {ShopifyAPI::Order.count(:status => "any")}
      puts "Total orders: #{count}"

      if count > 0
        page += count.divmod(10).first
        fail_array = Array.new

        while page > 0
          puts "Now processing page: #{page}"
          orders = ShopifyAPI::Session.temp("#{self.shopify_domain}", "#{self.shopify_token}") {ShopifyAPI::Order.find(:all, :params => {:status => "any", :page => page, :limit => 10})}
          orders.each do |order|
            params = JSON.parse(order.to_json)
            array_item = order_payload(params)
            orders_array.push(array_item)
            total_count += 1
          end

          payload = {
            "sales_invoices" => orders_array,
          }
          response = send_payload(ENV["SALES_BATCH_PATH"], payload, "order (batch)", "post")
          if response.code.to_s == "200"
            products_reconciliation_hash = {}
            response_array = JSON.parse(response.body)["sales_invoices"]
            response_array.each do |response_item|
              sales_invoice = response_item["sales_invoice"]
              if sales_invoice["status"] == 201
                success_count += 1

                transaction_lines = sales_invoice["content"]["transaction_lines_attributes"]
                transaction_lines.each do |item|
                  products_reconciliation_hash = add_or_initialize(products_reconciliation_hash, item["product"]["name"], item["quantity"].to_i)  
                end
              else
                fail_array.push(response_item)
              end
            end

            reconciliate_product(products_reconciliation_hash)
          end

          page -= 1
          orders_array.clear
        end

        unless fail_array.empty?
          order_log_item = SentDatum.find_by(kind: "Create order (batch)", shop_domain: "#{self.shopify_domain}")
          order_log_item.response = fail_array
          order_log_item.save
        end
      end

      update_sync_logs("Sync Orders", total_count, success_count, self.shopify_domain)
    end

    def reconciliate_product(hash)
      products_hash = JSON.parse(request_from_jurnal(ENV["PRODUCT_PATH"], self.shopify_domain).body)
      products = products_hash["products"]
      puts "Now Updating Products..."
      if !products.nil?
        if !products.empty?
          hash.each {|k, v|
            target = products.select {|x| x["name"] == k}
            if !target.empty?
              real_quantity = target[0]["init_quantity"].to_i + v
              quantity = {
                "init_quantity" => real_quantity
              }
              payload = {
                "product" => quantity
              }

              thepost = sendJSON("#{ENV["PRODUCT_PATH"]}/#{target[0]["id"]}", payload.to_json, self.shopify_domain, "put")
              puts "Response from Jurnal \n"
              puts "Response #{thepost.code} #{thepost.message} : #{thepost.body} \n"
            end
          }
        end
      end
    end

    def add_or_initialize(hash, key, value)
      if hash.select {|k, v| k == key}.empty?
        hash["#{key}"] = value
      else
        hash["#{key}"] += value
      end
      return hash
    end
end
