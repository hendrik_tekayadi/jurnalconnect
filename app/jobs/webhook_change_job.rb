class WebhookChangeJob < ActiveJob::Base
  include WebhookModule
  queue_as :default

  def perform(type, shop)
    Rails.cache.write("#{shop.shopify_domain}", self.job_id)
    if type == "remove"
      remove_webhooks(shop, self.job_id)
    else
      add_webhooks(shop, self.job_id)
    end
  end
end
