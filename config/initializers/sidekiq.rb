# # #for configuring the redis

Sidekiq.configure_server do |config|
 config.redis = { url: "redis://#{ENV['REDIS_SERVER']}:#{ENV['REDIS_PORT']}/#{ENV['REDIS_DATABASE']}" }
end

Sidekiq.configure_client do |config|
 config.redis = { url: "redis://#{ENV['REDIS_SERVER']}:#{ENV['REDIS_PORT']}/#{ENV['REDIS_DATABASE']}" }
end

Sidekiq.configure_server do |config|
  config.server_middleware do |chain|
    chain.remove Sidekiq::Middleware::Server::RetryJobs
  end
end